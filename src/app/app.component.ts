import { Component } from '@angular/core';
import { DataService } from './data.service';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
@Component({
  selector: 'app-root',
 /** template: `<h1>{{title}}</h1><h3>{{name}}</h3>
  <p>{{heroe}}</p> `,  multiple code templating**/
 /** template: `<img src="{{imgsrc}}"> <img [src]=imgsrc> <img bind-src=imgsrc>`, propert binging**/
 /** template: `<button [disabled]=bstatus>SEND</button>`, **/
  templateUrl: './app.commy.html',
 // styleUrls: ['./app.component.css']
 styles:[`
  h1{background-color:red;}
 .titleClass {background-color:#369!important;}
 .fontsiz {font-size:4em;}
 `],
 
 animations: [
 trigger('myAnimation',[
 state('small',style({
	 'tranform': 'scale(1)',
 })),
 state('large',style({
	 'tranform': 'scale(5)',
 })),
 transition('small <=> large',animate('1000ms ease-in',keyframes([
 style({opacity:0, tranform: 'translateY(-75%)',offset:0,color:'white','font-size':'4em'}),
 style({opacity:1, tranform: 'translateY(35%)',offset:0}),
 style({opacity:1, tranform: 'translateY(0)',offset:1,color:'red','font-size':'1em'})
 ]))),
 ]),
]
})
export class AppComponent {
	
  title = 'Application';
  name = "TESTING";
  imgsrc = "https://d30y9cdsu7xlg0.cloudfront.net/png/122-200.png";
  heroe: String;
  result: number;
  heroes = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado','RAZ'];
  heroclass : HeroClass =
  {
	  id: 1,
	  name: "raz"
  }
  
  bstatus = false;
  calc = 5;
  calc2 = 2;
  
   titleClasss = "titleClass"
   titleClass = true 
   titleClasses = {"titleClass":true,"fontsiz":true}
   
   msomestyles = {
	   "color":"blue",
	   "font-size":"2em"
	   }
  somestyle = "red"
  somestyles = true
  
  states: string = 'large'
  animateMe(){
	 
	 this.states = (this.states === 'small' ? 'large' : 'small')
  }
  /** CALCULATION EVENT **/
  KeyDowned(event)
  {
	  this.result = this.calc * this.calc2;
	// mytest : MyTest = new MyTest(this.calc,this.calc2);
   //  result = this.mytest.result; 
  }
  
  /** ARRAY ADD & REMOVE **/
  addelems : String = "";
  addElem = function()
  {
	//  alert(this.addelems);
	this.heroes.push(this.addelems)
  }
  removeItem = function(i)
  {
	  
	  this.heroes.splice(i,1);
  }
  
  dates = new Date(1992,1,30);
  
  /** SORT FOR PIPE **/
  sort_array = [6,9,7,8,4,5,1,2,3];
  /** TEMPLATE DRIVE FORM **/
  f_result : String = "";
  onSubmit = function(user)
  {
	  this.f_result = user.firstname+""+user.lastname+""+user.options;
	//  alert(user.firstname+""+);
  }
  m_result : String = "";
  
  /** MODEL DRIVE FORM **/
  onSubmitm = function(user)
  {
	  this.m_result = user.firstname+""+user.lastname+""+user.options;
	//  alert(user.firstname+""+);
  }

  form;
  
 mytest : MyTest = new MyTest(this.calc,this.calc2);
 // result = this.mytest.result;
 
 /** HTTP FUNCTION  **/
  https: Http;
  web_response : String = "";
 constructor(private dataService:DataService,private http:Http) {
    this.title = 'Tour of Heroes';
    this.heroe = 'Windstorm';
	this.https = http;
  }
  
  webreq =  function()
  {
	  
	  this.http.get("http://aliiff.com/webservice/notificationlist").map(response => response.json()).subscribe(data => {this.web_response = JSON.stringify(data)})
	 // console.log(this.http.get("http://aliiff.com/webservice/notificationlist"));
  }
  /** HTTP FUNCTION  **/
  servicestring: string;
  cars = [];
  ngOnInit()
  {
	  this.servicestring = this.dataService.ServiceDataFunction();
	  this.cars = this.dataService.cars;
	  this.dataService.cars[4] = "SOMETEST"
	  
	  /** MODEL DRIVEN FORM  **/
	  this.form = new FormGroup({
		  firstname : new FormControl("",Validators.compose([
		  Validators.required,
		  Validators.minLength(3),
		  Validators.pattern('[\\w-\\s\\/]+')
		  ])),
		  lastname : new FormControl("",this.LengthValidation),
		  options : new FormControl("",Validators.required)
	  })
  }
  
  LengthValidation(control)
  {
	  if(control.value.length < 4)
	  {
		  return {"lastname":true}
	  }
  }
   /* constructor() {
    this.title = 'Tour of Heroes';
    this.heroe = 'Windstorm';
	
  } */
}
export class HeroClass
{
	id: number;
	name: String;
}
export class MyTest
{
	calc: number;
	calc2: number;
	result : number;
	constructor(public calcc: number,
    public calc2c: number) {
    this.calc = calcc;
    this.calc2 = calc2c;
	this.result = calcc * calc2c;
  }
}
