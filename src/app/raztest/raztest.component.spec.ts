import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaztestComponent } from './raztest.component';

describe('RaztestComponent', () => {
  let component: RaztestComponent;
  let fixture: ComponentFixture<RaztestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaztestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaztestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
