import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms'
import { ReactiveFormsModule }   from '@angular/forms'
import { HttpModule }   from '@angular/http'
import { AppComponent } from './app.component';
import { RaztestComponent } from './raztest/raztest.component';
import { DataService } from './data.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductComponent } from './product/product.component';
import { RouterModule } from '@angular/router';
import { SortPipe } from './app.sort';

@NgModule({
  declarations: [
    SortPipe,
    AppComponent,
    RaztestComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
	FormsModule,
	ReactiveFormsModule,
	HttpModule,
	BrowserAnimationsModule,
	RouterModule.forRoot([
	{
		path: "raz",
		component: RaztestComponent
	},
	{
		path: "product",
		component: ProductComponent
	}
	])
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
